package org.asux.email;

import jakarta.mail.*;
import jakarta.mail.event.*;
import jakarta.mail.internet.*;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Properties;

/**
 * See ~/README-javax-jakarta-mail.md
 * Maven-Central and pom.xml at <a href="https://jakartaee.github.io/mail-api/README-JakartaMail#Download_Jakarta_Mail_Release">...</a>
 * HUMAN-FRIENDLY Explanation Documentation: <a href="https://jakarta.ee/specifications/mail/2.1/jakarta-mail-spec-2.1">...</a>
 * ---------------------
 * This project is part of Jakarta 10, Jakarta EE 9, and Jakarta EE 8.
 * Utility class to read emails stored on AWS-S3 in an encrypted-manner with a CMK.
 * Reading such emails is REALLY hard, and can --NOT-- be achieved via CLI or AWS-Console !!!
 */
public class EmailReader {

private final boolean DEBUG;

/**
 * Constructor
 * @param _debug boolean
 */
public EmailReader( final boolean _debug) {
    this.DEBUG = _debug;
} //// Constructor

//// ====================================================================================================

/**
 * This method reads the contents of a file (that was downloaded from S3-object).
 * The assumption is that the file is a MimeMessage file (i.e. a mail message file).
 * This method will EXTRACT all the components of the MimeMessage and store them as files in the same directory as the input file.
 *
 * @param filePath String
 */
public void downloadMailMimeAttachments( final String filePath ) {
    final java.nio.file.Path fp = java.nio.file.FileSystems.getDefault().getPath(filePath);
    // final java.nio.file.Path fp = new java.nio.file.Paths.of(filePath); <--- WRONG way to get a Path object.
    downloadMailMimeAttachments(fp);
} //// method downloadMailMimeAttachments()

//// ====================================================================================================

/**
 * Polymorphic method to @see downloadMailMimeAttachments(String)
 *
 * @param fp java.nio.file.Path (can NOT be null)
 */
public void downloadMailMimeAttachments( final java.nio.file.Path fp ) {
    // Get a Properties object
    Properties props = System.getProperties();

    // Get a Session object
    Session session = Session.getInstance(props, null);
    session.setDebug(DEBUG);
    try {
        System.out.println( "Reading MimeMessage from file: "+ fp +", where ATTACHMENTS will be stored as files with PREFIX='"+ fp.getFileName() +"'" );
        final MimeMessage mime_msg = new MimeMessage(session, new BufferedInputStream(new FileInputStream(fp.toFile())));
        EmailUtils.dumpPart( mime_msg, fp );
    } catch( FileNotFoundException | MessagingException e ) {
        e.printStackTrace();
        System.err.println( "\n\n!! FAILURE !! in READING/PARSING the MimeMessage\n" );
        System.exit(73);
    } catch( Exception e ) {
        e.printStackTrace();
        System.err.println( "\n\n!! FAILURE !! in printing MimeMessage\n" );
        System.exit(73);
    }
} //// method downloadMailMimeAttachments()

//// ====================================================================================================

/**
 * Currently NOTHING inside this.  To be used IN THE FUTURE for Unit-testing above code.
 * @param args String[] standard CLI-args
 */
public static void main(String[] args) {
} //// main()

} //// Class definition
