package org.asux.email;

import jakarta.mail.*;
import jakarta.mail.event.*;
import jakarta.mail.internet.*;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * See ~/README-javax-jakarta-mail.md
 * Maven-Central and pom.xml at <a href="https://jakartaee.github.io/mail-api/README-JakartaMail#Download_Jakarta_Mail_Release">...</a>
 * HUMAN-FRIENDLY Explanation Documentation: <a href="https://jakarta.ee/specifications/mail/2.1/jakarta-mail-spec-2.1">...</a>
 * ---------------------
 * This project is part of Jakarta 10, Jakarta EE 9, and Jakarta EE 8.
 * ---------------------
 * Code copied from <a href="https://jakarta.ee/specifications/mail/2.1/jakarta-mail-spec-2.1#a856">...</a>
 * Utility class to HANDLE the MIME-components within "regular" emails.
 * Only Static Methods in here.
 * Basically a compilation of all code out there on the web, to handle various scenarios.
 */
public class EmailUtils {

private EmailUtils() {
    // This class is not meant to be instantiated.
}

/**
 * This is a flag that controls whether to --PRINT-OUT-- explanatory-details about the structure of the MIME-message or not.
 */
public static boolean showStructure = true;

//// ====================================================================================================
//// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
//// ====================================================================================================

/**
 * This method reads the contents of a file (that was downloaded from S3-object, created by AWS-SES).
 * The assumption is that the file is a MimeMessage file (i.e. a mail message file).
 * This method will EXTRACT all the components of the MimeMessage and store them as files in the same directory as the input file.
 *
 * @param p jakarta.mail.Part
 * @param fileNamePrefix java.nio.file.Path
 * @throws Exception if any corruption in the MIME-message
 */
public static void dumpPart(Part p, final java.nio.file.Path fileNamePrefix) throws Exception {
    if (p instanceof Message)
        dumpEnvelope((Message)p);

    /* Dump input stream ..
         InputStream is = p.getInputStream();
         // If "is" is not already buffered, wrap a BufferedInputStream
         // around it.
         if (!(is instanceof BufferedInputStream))
         is = new BufferedInputStream(is);
         int c;
         while ((c = is.read()) != -1)
         System.out.write(c);

     */

    int level = 0;
    int attnum = 1;
    String ct = p.getContentType();
    try {
        System.out.println("CONTENT-TYPE: " + (new ContentType(ct)));
    } catch (ParseException pex) {
        System.out.println("BAD CONTENT-TYPE: " + ct);
    }
    String filename = p.getFileName();
    if (filename != null)
        System.out.println("Embedded FILENAME (within Mime): " + filename);

    /*
     * Using isMimeType to determine the content type avoids
     * fetching the actual content data until we need it.
     */
    if (p.isMimeType("text/plain")) {
        System.out.println("This is plain text");
        System.out.println("---------------------------");
        System.out.println((String)p.getContent());
    } else if (p.isMimeType("multipart/*")) {
        System.out.println("This is a Multipart");
        System.out.println("---------------------------");
        Multipart mp = (Multipart)p.getContent();
        level++;
        int count = mp.getCount();
        for (int i = 0; i < count; i++)
            dumpPart( mp.getBodyPart(i), fileNamePrefix );
        level--;
    } else if (p.isMimeType("message/rfc822")) {
        System.out.println("This is a Nested Message");
        System.out.println("---------------------------");
        level++;
        dumpPart( (Part)p.getContent(), fileNamePrefix );
        level--;
    } else {
        if (!showStructure) {
            //// If we actually want to see the data, and it's not a
            //// MIME type we know, fetch it and check its Java type.
            Object o = p.getContent();
            if (o instanceof String) {
                System.out.println("This is a string");
                System.out.println("---------------------------");
                System.out.println((String)o);
            } else if (o instanceof InputStream is) {
                System.out.println("This is just an input stream");
                System.out.println("---------------------------");
                int c;
                while ((c = is.read()) != -1)
                    System.out.write(c);
            } else {
                System.out.println("This is an unknown type");
                System.out.println("---------------------------");
                System.out.println(o.toString());
            }
        } else {
            // just a separator
            System.out.println("---------------------------");
        }
    }

    /*
     * If we're saving attachments, write out anything that
     * looks like an attachment into an appropriately named
     * file.  Don't overwrite existing files to prevent
     * mistakes.
     */
    if ( p instanceof MimeBodyPart ) {
        System.out.println( "Mime-Part's getContentType='"+ p.getContentType() +"'" );
    // if ( p instanceof MimeBodyPart && !p.isMimeType("multipart/*") ) {
    // if ( level != 0 && p instanceof MimeBodyPart && !p.isMimeType("multipart/*") ) {
        String disp = p.getDisposition();
        // many mailers don't include a Content-Disposition
        if (disp == null || disp.equalsIgnoreCase(Part.ATTACHMENT)) {
            if (filename == null)
                filename = fileNamePrefix.toString() +'-'+ attnum++;
            if ( p.getContentType().startsWith("text/html;") )
                filename += ".html";
            System.out.println("===================================================================================================");
            System.out.println("Saving attachment to file " + filename);
            try {
                File f = new File(filename);
                if (f.exists())
                    // XXX - could try a series of names
                    throw new IOException("file exists");
                ((MimeBodyPart)p).saveFile(f);
            } catch (IOException ex) {
                System.out.println("Failed to save attachment: " + ex);
            }
            System.out.println("---------------------------");
        }
    }
} //// method dumpPart()

//// ====================================================================================================
//// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
//// ====================================================================================================

/**
 * This method dumps out the envelope (That is the "main" fields of an email:- from, to, cc, subject, received date,
 * sent date, etc.)
 *
 * @param m Message
 * @throws Exception if any corruption in the MIME-message
 */
public static void dumpEnvelope(Message m) throws Exception {
    System.out.println("This is the message envelope");
    System.out.println("---------------------------");
    Address[] a;
    // FROM
    if ((a = m.getFrom()) != null) {
        for (int j = 0; j < a.length; j++)
            System.out.println("FROM: " + a[j].toString());
    }

    // REPLY TO
    if ((a = m.getReplyTo()) != null) {
        for (int j = 0; j < a.length; j++)
            System.out.println("REPLY TO: " + a[j].toString());
    }

    // TO
    if ((a = m.getRecipients(Message.RecipientType.TO)) != null) {
        for (int j = 0; j < a.length; j++) {
            System.out.println("TO: " + a[j].toString());
            InternetAddress ia = (InternetAddress)a[j];
            if (ia.isGroup()) {
                InternetAddress[] aa = ia.getGroup(false);
                for (int k = 0; k < aa.length; k++)
                    System.out.println("  GROUP: " + aa[k].toString());
            }
        }
    }

    // SUBJECT
    System.out.println("SUBJECT: " + m.getSubject());

    // DATE
    java.util.Date d = m.getSentDate();
    System.out.println("SendDate: " +
            (d != null ? d.toString() : "UNKNOWN"));

    // FLAGS
    Flags flags = m.getFlags();
    StringBuffer sb = new StringBuffer();
    Flags.Flag[] sf = flags.getSystemFlags(); // get the system flags

    boolean first = true;
    for (int i = 0; i < sf.length; i++) {
        String s;
        Flags.Flag f = sf[i];
        if (f == Flags.Flag.ANSWERED)
            s = "\\Answered";
        else if (f == Flags.Flag.DELETED)
            s = "\\Deleted";
        else if (f == Flags.Flag.DRAFT)
            s = "\\Draft";
        else if (f == Flags.Flag.FLAGGED)
            s = "\\Flagged";
        else if (f == Flags.Flag.RECENT)
            s = "\\Recent";
        else if (f == Flags.Flag.SEEN)
            s = "\\Seen";
        else
            continue;       // skip it
        if (first)
            first = false;
        else
            sb.append(' ');
        sb.append(s);
    }

    String[] uf = flags.getUserFlags(); // get the user flag strings
    for (int i = 0; i < uf.length; i++) {
        if (first)
            first = false;
        else
            sb.append(' ');
        sb.append(uf[i]);
    }
    System.out.println("FLAGS: " + sb);

    // X-MAILER
    String[] hdrs = m.getHeader("X-Mailer");
    if (hdrs != null)
        System.out.println("X-Mailer: " + hdrs[0]);
    else
        System.out.println("X-Mailer NOT available");
} //// Method dumpEnvelope()


//// ====================================================================================================
//// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
//// ====================================================================================================

}
