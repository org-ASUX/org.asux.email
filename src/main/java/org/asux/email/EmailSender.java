package org.asux.email;

import jakarta.mail.*;

import jakarta.mail.internet.*;

import jakarta.activation.DataHandler;
import jakarta.activation.DataSource;
import jakarta.activation.FileDataSource;

import java.util.Properties;
import java.util.function.BiConsumer;

/**
 * See ~/README-javax-jakarta-mail.md
 * Maven-Central and pom.xml at <a href="https://jakartaee.github.io/mail-api/README-JakartaMail#Download_Jakarta_Mail_Release">...</a>
 * HUMAN-FRIENDLY Explanation Documentation: <a href="https://jakarta.ee/specifications/mail/2.1/jakarta-mail-spec-2.1">...</a>
 * ---------------------
 * This project is part of Jakarta 10, Jakarta EE 9, and Jakarta EE 8.
 * Utility class to --SEND--OUT-- emails with MIME-attachments.
 * Basically a compilation of all code out there on the web, to handle various scenarios.
 */
public class EmailSender {

/**
 * The SMTP_HOST and SMTP_PORT are the default values for the SMTP server.
 * The SMTP_USERNAME and SMTP_PASSWORD are the credentials to login to the SMTP server.
 * TODO These values SHOULD be in the System.properties.
 */
private static final String SMTP_HOST = "smtp.mailtrap.io";
private static final String SMTP_PORT = "587";

private final boolean DEBUG;

/**
 * Constructor
 * @param _debug boolean
 */
public EmailSender ( final boolean _debug) {
    this.DEBUG = _debug;
} //// Constructor
//// ====================================================================================================

/**
 * This method SENDS OUT email (via SMTP) with MIME attachments.
 *
 * @param fromEmailAddr String the email address of the sender
 * @param toEmailAddr String the email address of the recipient
 * @param subjectOfEmail String the subject of the email
 * @param messageBodyAsIs String the body of the email
 * @param attachmentFilePaths String[] the possibly EMPTY LIST of file-paths representing MIME-attachments to be sent
 */
public void sendEmailWithMimeAttachments(
        final String fromEmailAddr,
        final String toEmailAddr,
        final String subjectOfEmail,
        final String messageBodyAsIs,
    final String[] attachmentFilePaths
) {
    java.nio.file.Path[] fps = new java.nio.file.Path[attachmentFilePaths.length];
    int ix = 0;
    for ( String filePath : attachmentFilePaths ) {
        final java.nio.file.Path fp = java.nio.file.FileSystems.getDefault().getPath(filePath);
        // final java.nio.file.Path fp = new java.nio.file.Paths.of(filePath); <--- WRONG way to get a Path object.
        fps[ix++] = fp;
    } //// For loop
    sendEmailWithMimeAttachments( fromEmailAddr, toEmailAddr, subjectOfEmail, messageBodyAsIs, fps);
} //// method downloadMailMimeAttachments()

/**
 * Poluymorphic method to @see sendEmailWithMimeAttachments(String, String, String, String, String[])
 * @param fromEmailAddr String the email address of the sender
 * @param toEmailAddr String the email address of the recipient
 * @param subjectOfEmail String the subject of the email
 * @param messageBodyAsIs String the body of the email
 * @param attachmentFilePaths java.nio.file.Path[] the possibly EMPTY LIST of file-paths representing MIME-attachments to be sent
 */
public void sendEmailWithMimeAttachments(
        final String fromEmailAddr,
        final String toEmailAddr,
        final String subjectOfEmail,
        final String messageBodyAsIs,
        final java.nio.file.Path[] attachmentFilePaths
) {

    // Get a Properties object
    Properties props = System.getProperties();
    Properties sendProps = new Properties();

    //// ------------------- inline Common Code / Macro
    final BiConsumer<String,String> setSendMailProperties = ( String mailProperty, String defaultValue) -> {
        if ( props.getProperty( mailProperty ) != null )
            sendProps.put( mailProperty, defaultValue );
        else
            sendProps.put( mailProperty, props.getProperty(mailProperty) );
    };  //// end of INLINE macro

    setSendMailProperties.accept("mail.smtp.auth", "true" );
    setSendMailProperties.accept("mail.smtp.starttls.enable", "true");
    setSendMailProperties.accept("mail.smtp.host", SMTP_HOST );
    setSendMailProperties.accept("mail.smtp.port", SMTP_PORT );

    final String SMTP_USERNAME = props.getProperty("SMTP_USERNAME");
    final String SMTP_PASSWORD = props.getProperty("SMTP_PASSWORD");

    Authenticator authenticator = new Authenticator() {
        protected PasswordAuthentication getPasswordAuthentication() {
            return new PasswordAuthentication(SMTP_USERNAME, SMTP_PASSWORD);
        }
    };

    // Get the Session object.
    Session session = Session.getInstance( sendProps, authenticator );
    session.setDebug(DEBUG);


    try {
        // Create a default MimeMessage object
        Message message = new MimeMessage(session);     //// https://jakarta.ee/specifications/mail/2.1/jakarta-mail-spec-2.1#the-mimemessage-class
        message.setFrom(new InternetAddress( fromEmailAddr ));
        message.setRecipients( Message.RecipientType.TO, InternetAddress.parse( toEmailAddr ) );
        message.setSubject( subjectOfEmail );

        Multipart multipart = new MimeMultipart();  //// REF: https://jakarta.ee/specifications/mail/2.1/jakarta-mail-spec-2.1#building-a-mime-multipart-message

        BodyPart messageMainBodyPart = new MimeBodyPart();
        //// Which to use?  setText vs. setContent?   REF: https://jakarta.ee/specifications/mail/2.1/jakarta-mail-spec-2.1#a597
        ////    FYI: setContent() automatically creates the DataHandler object for you.
        messageMainBodyPart.setText( messageBodyAsIs );
        // ???? messageMainBodyPart.setContent( messageBodyAsIs, "text/plain; charset=UTF-8" );
        // ???? messageMainBodyPart.setContent( messageBodyAsIs, "text/html; charset=UTF-8" );
        multipart.addBodyPart(messageMainBodyPart);

        for ( java.nio.file.Path attachmentFilePath : attachmentFilePaths) {
            System.out.println("Reading MimeMessage from file: " + attachmentFilePath + ", where ATTACHMENTS will be stored as files with PREFIX='" + attachmentFilePath.getFileName() + "'");

            final BodyPart attachmentBodyPart = new MimeBodyPart();
            DataSource source = new FileDataSource( attachmentFilePath.toFile() );      //// https://jakarta.ee/specifications/activation/2.1/apidocs/jakarta.activation/jakarta/activation/datasource
            attachmentBodyPart.setDataHandler(new DataHandler(source));                 //// https://jakarta.ee/specifications/activation/2.1/apidocs/jakarta.activation/jakarta/activation/datahandler
            //// NOTE: The _Jakarta-Mail_ DEPENDS heavily on _Jakarta-Activation_ to __AUTOMATICALLY__ determine the MIME data type!
            /// That is exactly what happens AUTOMATICALLY when DataHandler is created with a DataSource (as shown above)
            attachmentBodyPart.setFileName( attachmentFilePath.getFileName().toString() );

            final String type = attachmentBodyPart.getContentType();
            final ContentType cType = new ContentType(type);    //// https://jakarta.ee/specifications/mail/2.1/apidocs/jakarta.mail/jakarta/mail/internet/contenttype
            if (cType.match("application/"))                 //// https://jakarta.ee/specifications/mail/2.1/apidocs/jakarta.mail/jakarta/mail/internet/contenttype#match(java.lang.String)
                System.out.println( "👉🏾 Attention!! Attachment '"+ attachmentFilePath +"' is of type: '" + cType.getBaseType() +"'" );
            else
                System.out.println( "FYI: Attachment '"+ attachmentFilePath +"' is of type: '" + cType.getBaseType() +"'" );

            multipart.addBodyPart(attachmentBodyPart);

        } //// For loop

        message.setContent(multipart);  // All content now added to the message

        Transport.send(message);        // Send message
        System.out.println("✅ Sent message successfully....");

    } catch ( MessagingException e ) {
        e.printStackTrace();
        System.err.println("\n\n!! FAILURE ❌❌❌ !! in Creating a MimeMessage\n");
        System.exit(73);
    } catch ( Exception e ) {
        e.printStackTrace();
        System.err.println("\n\n!! FAILURE ❌❌❌ !! in printing MimeMessage\n");
        System.exit(73);
    }
} //// method sendEmailWithMimeAttachments()

//// ====================================================================================================
//// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
//// ====================================================================================================

/**
 * Currently NOTHING inside this.  To be used IN THE FUTURE for Unit-testing above code.
 * @param args String[] standard CLI-args
 */
public static void main(String[] args) {
} //// main()

} //// Class definition
